/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Visao;

import java.awt.Color;

/**
 *
 * @author Usuário
 */
public class JanelaConfiguracao extends javax.swing.JFrame {
    
    private final Color dia = Color.WHITE;
    private final Color noite = Color.GRAY;
    private final Color Luar = Color.LIGHT_GRAY;

    /**
     * Creates new form JFrameConfiguracao
     */
    public JanelaConfiguracao() {
        initComponents();
        buttonGroupAparencia.add(jRadioButtonDia);
        buttonGroupAparencia.add(jRadioButtonNoite);
        buttonGroupAparencia.add(jRadioButtonLuar);
        jRadioButtonDia.setSelected(true);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroupAparencia = new javax.swing.ButtonGroup();
        jButtonOK = new javax.swing.JButton();
        jButtonAplicar = new javax.swing.JButton();
        jButtonCancelar = new javax.swing.JButton();
        jTabbedPane = new javax.swing.JTabbedPane();
        jPanelAparencia = new javax.swing.JPanel();
        jRadioButtonDia = new javax.swing.JRadioButton();
        jRadioButtonNoite = new javax.swing.JRadioButton();
        jRadioButtonLuar = new javax.swing.JRadioButton();
        jPanelBaseDados = new javax.swing.JPanel();
        jPanelConexao = new javax.swing.JPanel();
        jLabelHost = new javax.swing.JLabel();
        jLabelPorta = new javax.swing.JLabel();
        jLabelUsuario = new javax.swing.JLabel();
        jLabelSenha = new javax.swing.JLabel();
        jLabelBase = new javax.swing.JLabel();
        jTextFieldHost = new javax.swing.JTextField();
        jTextFieldPorta = new javax.swing.JTextField();
        jTextFieldUsuario = new javax.swing.JTextField();
        jPasswordField = new javax.swing.JPasswordField();
        jTextFieldBase = new javax.swing.JTextField();
        jButtonTestar = new javax.swing.JButton();
        jPanelSGBD = new javax.swing.JPanel();
        jLabelSGBD = new javax.swing.JLabel();
        jLabelCaminho = new javax.swing.JLabel();
        jTextFieldSGBD = new javax.swing.JTextField();
        jTextFieldCaminho = new javax.swing.JTextField();
        jButtonLocalizar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jButtonOK.setText("OK");
        jButtonOK.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonOKActionPerformed(evt);
            }
        });

        jButtonAplicar.setText("Aplicar");
        jButtonAplicar.setEnabled(false);
        jButtonAplicar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAplicarActionPerformed(evt);
            }
        });

        jButtonCancelar.setText("Cancelar");
        jButtonCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonCancelarActionPerformed(evt);
            }
        });

        jPanelAparencia.setBackground(new java.awt.Color(255, 255, 255));

        jRadioButtonDia.setBackground(new java.awt.Color(255, 255, 255));
        jRadioButtonDia.setText("Dia");
        jRadioButtonDia.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButtonDiaActionPerformed(evt);
            }
        });

        jRadioButtonNoite.setBackground(new java.awt.Color(255, 255, 255));
        jRadioButtonNoite.setText("Noite");
        jRadioButtonNoite.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButtonNoiteActionPerformed(evt);
            }
        });

        jRadioButtonLuar.setBackground(new java.awt.Color(255, 255, 255));
        jRadioButtonLuar.setText("Luar");
        jRadioButtonLuar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jRadioButtonLuarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanelAparenciaLayout = new javax.swing.GroupLayout(jPanelAparencia);
        jPanelAparencia.setLayout(jPanelAparenciaLayout);
        jPanelAparenciaLayout.setHorizontalGroup(
            jPanelAparenciaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelAparenciaLayout.createSequentialGroup()
                .addContainerGap(164, Short.MAX_VALUE)
                .addComponent(jRadioButtonDia, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jRadioButtonNoite, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jRadioButtonLuar, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(176, Short.MAX_VALUE))
        );
        jPanelAparenciaLayout.setVerticalGroup(
            jPanelAparenciaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelAparenciaLayout.createSequentialGroup()
                .addContainerGap(168, Short.MAX_VALUE)
                .addGroup(jPanelAparenciaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jRadioButtonDia)
                    .addComponent(jRadioButtonNoite)
                    .addComponent(jRadioButtonLuar))
                .addContainerGap(184, Short.MAX_VALUE))
        );

        jTabbedPane.addTab("Aparência", jPanelAparencia);

        jPanelBaseDados.setBackground(new java.awt.Color(255, 255, 255));

        jPanelConexao.setBackground(new java.awt.Color(255, 255, 255));
        jPanelConexao.setBorder(javax.swing.BorderFactory.createTitledBorder("Conexão"));

        jLabelHost.setText("Host");

        jLabelPorta.setText("Porta");

        jLabelUsuario.setText("Usuário");

        jLabelSenha.setText("Senha");

        jLabelBase.setText("Base");

        jPasswordField.setText("jPasswordField1");

        jButtonTestar.setText("Testar");

        javax.swing.GroupLayout jPanelConexaoLayout = new javax.swing.GroupLayout(jPanelConexao);
        jPanelConexao.setLayout(jPanelConexaoLayout);
        jPanelConexaoLayout.setHorizontalGroup(
            jPanelConexaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelConexaoLayout.createSequentialGroup()
                .addGap(45, 45, 45)
                .addGroup(jPanelConexaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabelBase)
                    .addComponent(jLabelUsuario)
                    .addComponent(jLabelHost))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanelConexaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jTextFieldBase, javax.swing.GroupLayout.DEFAULT_SIZE, 140, Short.MAX_VALUE)
                    .addGroup(jPanelConexaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(jTextFieldHost)
                        .addComponent(jTextFieldUsuario, javax.swing.GroupLayout.DEFAULT_SIZE, 140, Short.MAX_VALUE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanelConexaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelConexaoLayout.createSequentialGroup()
                        .addGroup(jPanelConexaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabelSenha, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabelPorta, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanelConexaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jTextFieldPorta, javax.swing.GroupLayout.DEFAULT_SIZE, 140, Short.MAX_VALUE)
                            .addComponent(jPasswordField)))
                    .addComponent(jButtonTestar, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(46, 46, 46))
        );
        jPanelConexaoLayout.setVerticalGroup(
            jPanelConexaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelConexaoLayout.createSequentialGroup()
                .addContainerGap(27, Short.MAX_VALUE)
                .addGroup(jPanelConexaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelHost)
                    .addComponent(jTextFieldHost, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabelPorta)
                    .addComponent(jTextFieldPorta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanelConexaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelConexaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jTextFieldUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabelUsuario))
                    .addGroup(jPanelConexaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabelSenha)
                        .addComponent(jPasswordField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addGroup(jPanelConexaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelConexaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabelBase)
                        .addComponent(jTextFieldBase, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jButtonTestar))
                .addContainerGap(49, Short.MAX_VALUE))
        );

        jPanelSGBD.setBackground(new java.awt.Color(255, 255, 255));
        jPanelSGBD.setBorder(javax.swing.BorderFactory.createTitledBorder("Sistema Gerenciador de Banco de Dados"));

        jLabelSGBD.setText("SGBD");

        jLabelCaminho.setText("Caminho");

        jButtonLocalizar.setText("Localizar");

        javax.swing.GroupLayout jPanelSGBDLayout = new javax.swing.GroupLayout(jPanelSGBD);
        jPanelSGBD.setLayout(jPanelSGBDLayout);
        jPanelSGBDLayout.setHorizontalGroup(
            jPanelSGBDLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelSGBDLayout.createSequentialGroup()
                .addGap(40, 40, 40)
                .addGroup(jPanelSGBDLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabelCaminho)
                    .addComponent(jLabelSGBD))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanelSGBDLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanelSGBDLayout.createSequentialGroup()
                        .addComponent(jTextFieldCaminho)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED))
                    .addGroup(jPanelSGBDLayout.createSequentialGroup()
                        .addComponent(jTextFieldSGBD, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(164, 164, 164)))
                .addComponent(jButtonLocalizar, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(46, 46, 46))
        );
        jPanelSGBDLayout.setVerticalGroup(
            jPanelSGBDLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanelSGBDLayout.createSequentialGroup()
                .addContainerGap(24, Short.MAX_VALUE)
                .addGroup(jPanelSGBDLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelSGBD)
                    .addComponent(jTextFieldSGBD, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanelSGBDLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextFieldCaminho, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabelCaminho)
                    .addComponent(jButtonLocalizar))
                .addContainerGap(20, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanelBaseDadosLayout = new javax.swing.GroupLayout(jPanelBaseDados);
        jPanelBaseDados.setLayout(jPanelBaseDadosLayout);
        jPanelBaseDadosLayout.setHorizontalGroup(
            jPanelBaseDadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelBaseDadosLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanelBaseDadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanelSGBD, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanelConexao, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanelBaseDadosLayout.setVerticalGroup(
            jPanelBaseDadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelBaseDadosLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanelSGBD, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanelConexao, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        jTabbedPane.addTab("Base de dados", jPanelBaseDados);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jButtonOK, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButtonCancelar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButtonAplicar, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jTabbedPane))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTabbedPane)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButtonOK)
                    .addComponent(jButtonAplicar)
                    .addComponent(jButtonCancelar))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonOKActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonOKActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButtonOKActionPerformed

    private void jButtonAplicarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAplicarActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButtonAplicarActionPerformed

    private void jButtonCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonCancelarActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButtonCancelarActionPerformed

    private void jRadioButtonDiaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButtonDiaActionPerformed
        jPanelBaseDados.setBackground(dia);
        jPanelAparencia.setBackground(dia);
        jRadioButtonDia.setBackground(dia);
        jRadioButtonNoite.setBackground(dia);
        jRadioButtonLuar.setBackground(dia);
    }//GEN-LAST:event_jRadioButtonDiaActionPerformed

    private void jRadioButtonNoiteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButtonNoiteActionPerformed
        jPanelBaseDados.setBackground(noite);
        jPanelAparencia.setBackground(noite);
        jRadioButtonDia.setBackground(noite);
        jRadioButtonNoite.setBackground(noite);
        jRadioButtonLuar.setBackground(noite);
    }//GEN-LAST:event_jRadioButtonNoiteActionPerformed

    private void jRadioButtonLuarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jRadioButtonLuarActionPerformed
        jPanelBaseDados.setBackground(Luar);
        jPanelAparencia.setBackground(Luar);
        jRadioButtonDia.setBackground(Luar);
        jRadioButtonNoite.setBackground(Luar);
        jRadioButtonLuar.setBackground(Luar);
    }//GEN-LAST:event_jRadioButtonLuarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(JanelaConfiguracao.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new JanelaConfiguracao().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroupAparencia;
    private javax.swing.JButton jButtonAplicar;
    private javax.swing.JButton jButtonCancelar;
    private javax.swing.JButton jButtonLocalizar;
    private javax.swing.JButton jButtonOK;
    private javax.swing.JButton jButtonTestar;
    private javax.swing.JLabel jLabelBase;
    private javax.swing.JLabel jLabelCaminho;
    private javax.swing.JLabel jLabelHost;
    private javax.swing.JLabel jLabelPorta;
    private javax.swing.JLabel jLabelSGBD;
    private javax.swing.JLabel jLabelSenha;
    private javax.swing.JLabel jLabelUsuario;
    private javax.swing.JPanel jPanelAparencia;
    private javax.swing.JPanel jPanelBaseDados;
    private javax.swing.JPanel jPanelConexao;
    private javax.swing.JPanel jPanelSGBD;
    private javax.swing.JPasswordField jPasswordField;
    private javax.swing.JRadioButton jRadioButtonDia;
    private javax.swing.JRadioButton jRadioButtonLuar;
    private javax.swing.JRadioButton jRadioButtonNoite;
    private javax.swing.JTabbedPane jTabbedPane;
    private javax.swing.JTextField jTextFieldBase;
    private javax.swing.JTextField jTextFieldCaminho;
    private javax.swing.JTextField jTextFieldHost;
    private javax.swing.JTextField jTextFieldPorta;
    private javax.swing.JTextField jTextFieldSGBD;
    private javax.swing.JTextField jTextFieldUsuario;
    // End of variables declaration//GEN-END:variables
}
